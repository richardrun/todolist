package com.example.richa.todolist.CalendarActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.example.richa.todolist.GeneralFunction.GeneralFunctions;
import com.example.richa.todolist.MenuRecyclerView.MenuRecyclerViewAdapter;
import com.example.richa.todolist.MenuRecyclerView.Task;
import com.example.richa.todolist.R;
import com.example.richa.todolist.RealmDBOperation.RealmDBOperation;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by richa on 5/29/2017.
 */

public class CalendarActivity extends AppCompatActivity {

    GeneralFunctions genFunc;
    Realm realm;
    public static RealmDBOperation realmDB;

    List<Task> taskList;
    MenuRecyclerViewAdapter adapter;

    @BindView(R.id.calendarView) MaterialCalendarView widget;
    @BindView(R.id.menu_rv) RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);
        ButterKnife.bind(this);
        genFunc = new GeneralFunctions();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        realm = Realm.getInstance(config);
        //realm = Realm.getDefaultInstance();
        realmDB = RealmDBOperation.getInstance(realm);

        //init rv
        initializeData();



        EventDecorator eventDecorator = new EventDecorator(50,dotInit());
        widget.addDecorator(eventDecorator);

        widget.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Log.e("ERROR", "the selected date: " + date.toString());
                Log.e("ERROR", "the selected date: " + date.getMonth());

                String monthString = genFunc.monthIntToString(date.getMonth());
                Log.e("ERROR", "month string: " + monthString);

                adapter.deleteAll();

                List<Task> result = realmDB.getTitleByDate(realm,monthString,Integer.toString(date.getDay()),Integer.toString(date.getYear()));
                Log.e("ERROR", "result size: " + result.size());
                if (result.size()>0){
                    for(Task task:result){
                        adapter.add(task);
                    }
                }


            }
        });

    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    /**
     * rv init
     */
    private void initializeData(){
        rv=(RecyclerView)findViewById(R.id.menu_rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        taskList = new ArrayList<>();

        adapter = new MenuRecyclerViewAdapter(taskList);
        rv.setAdapter(adapter);
    }

    public HashSet<CalendarDay> dotInit(){



        HashSet<CalendarDay> hs = new HashSet<>();

        List<Task> result = realmDB.findAll(realm);

        Log.e("Calendar view:", "size of db: "+ result.size());
        if(result.size()>0){
            for(Task task:result){
                int year = -1;
                int month = -1;
                int day = -1;

                Log.e("Calendar view:", "year before"+ task.getYear());

                if(task.getYear()!=null){
                    year = Integer.parseInt(task.getYear());
                }

                if(task.getMonth()!=null){
                    month = genFunc.monthStringToInt(task.getMonth());
                }

                if(task.getDay()!=null){
                    day = Integer.parseInt(task.getDay());
                }

                Log.e("calendar view  init", "year: " + year);

                if(year!=-1){
                    CalendarDay calDay = CalendarDay.from(year,month,day);
                    hs.add(calDay);
                }


            }
        }
        return hs;
    }

    @OnClick(R.id.listFAB)
    public void listFAB(){
        finish();
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }
}