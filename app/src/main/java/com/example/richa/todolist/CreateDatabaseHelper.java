package com.example.richa.todolist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by richa on 4/8/2017.
 */

public class CreateDatabaseHelper extends SQLiteOpenHelper {

    public CreateDatabaseHelper(Context con){
        super(con,"database", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
