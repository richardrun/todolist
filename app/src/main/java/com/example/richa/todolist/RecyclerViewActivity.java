package com.example.richa.todolist;

/**
 * Created by richa on 5/11/2017.
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.example.richa.todolist.PersonContactData.Person;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecyclerViewActivity extends AppCompatActivity {

    private List<Person> persons;
    private RecyclerView rv;
    RVAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recycleview);
        ButterKnife.bind(this);

        rv=(RecyclerView)findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        initializeData();
        initializeAdapter();
    }

    private void initializeData(){
        persons = new ArrayList<>();
       // persons.add(new Person("Emma Wilson", "23 years old"));
        //persons.add(new Person("Lavery Maiss", "25 years old"));
       // persons.add(new Person("Lillie Watts", "35 years old"));
    }

    private void initializeAdapter(){
        adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);
    }

    @Nullable
    @OnClick(R.id.add_persons)
    public void addPerson(Button button){
        //persons.add(new Person("nest add", "35 years old"));
        adapter.notifyItemInserted(persons.size()-1);
        adapter.notifyItemRangeChanged(persons.size()-1,adapter.getItemCount());
    }
}