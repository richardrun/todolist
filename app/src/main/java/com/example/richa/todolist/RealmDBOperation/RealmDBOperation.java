package com.example.richa.todolist.RealmDBOperation;

import android.util.Log;

import com.example.richa.todolist.Menu;
import com.example.richa.todolist.MenuRecyclerView.Task;
import com.example.richa.todolist.PersonContactData.Person;
import com.example.richa.todolist.PersonContactData.PersonRealm;
import com.example.richa.todolist.RVAdapter;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;

/**
 * Created by richa on 5/18/2017.
 */

public class RealmDBOperation {

    public static RealmDBOperation db = null;

    private RealmDBOperation(){}

    public static RealmDBOperation getInstance(Realm realm){
        if(db == null){
            db = new RealmDBOperation();
        }
        return db;
    }

    public void add(Realm realm, String id, final TaskNotRealmObject tempTask) {
        final Task task = new Task();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                task.setId(tempTask.getId());
                task.setTitle(tempTask.getTitle());
                task.setContent(tempTask.getContent());
                task.setLocationOn(tempTask.getLocationOn());
                task.setLocation(tempTask.getLocation());

                task.setNotification(tempTask.getNotification());
                task.setDateString(tempTask.getDateString());
                task.setMonth(tempTask.getMonth());
                task.setDay(tempTask.getDay());
                task.setYear(tempTask.getYear());
                task.setTimeString(tempTask.getTimeString());
                task.setType(tempTask.getType());
                task.setComplete(tempTask.getComplete());


                //person contact
                List<Person> persons = RVAdapter.getPersonsList();

                for(Person person:persons){
                    PersonRealm personRealm = new PersonRealm();
                    personRealm.updateStates(person);
                    personRealm.parentsID = tempTask.getId();
                    personRealm.setKey();
                    realm.createObject(PersonRealm.class,personRealm.key);
                    task.personsRealmList.add(personRealm);
                }

                realm.copyToRealmOrUpdate(task);
            }
        });
    }

    public PersonRealm addPerson(Realm realm, final Person person){
        PersonRealm personRealm = new PersonRealm();
        personRealm.updateStates(person);
        realm.copyToRealmOrUpdate(personRealm);

        return personRealm;
    }

    public void updateAll(Realm realm, final TaskNotRealmObject tempTask){
        final Task task = getTaskById(realm, Integer.parseInt(tempTask.getId()));
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                task.setTitle(tempTask.getTitle());
                task.setContent(tempTask.getContent());
                task.setLocationOn(tempTask.getLocationOn());
                task.setLocation(tempTask.getLocation());

                task.setNotification(tempTask.getNotification());
                task.setDateString(tempTask.getDateString());
                task.setMonth(tempTask.getMonth());
                task.setDay(tempTask.getDay());
                task.setYear(tempTask.getYear());
                task.setTimeString(tempTask.getTimeString());
                task.setType(tempTask.getType());
                task.setComplete(tempTask.getComplete());

                List<Person> persons = RVAdapter.getPersonsList();
                Log.e("ERROR", "persons's size: " + persons.size());
                Log.e("ERROR", "task's size: " + task.personsRealmList.size());
                task.personsRealmList.clear();
                RealmResults<PersonRealm>  result = realm.where(PersonRealm.class).equalTo("parentsID",task.getId()).findAll();
                result.deleteAllFromRealm();
                Log.e("ERROR", "task's size after: " + task.personsRealmList.size());

                for(Person person:persons){
                    PersonRealm personRealm = new PersonRealm();
                    personRealm.updateStates(person);
                    personRealm.parentsID = tempTask.getId();
                    personRealm.setKey();
                    realm.createObject(PersonRealm.class,personRealm.key);
                    task.personsRealmList.add(personRealm);
                }

            }
        });
    }

    public void updateTitle(Realm realm, final List<Task> taskList, final int id, final String title){
        final Task task = getTaskById(realm, id);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                task.setTitle(title);
                taskList.get(id).setTitle(title);
                //realm.copyToRealmOrUpdate(task);
            }
        });
    }

    public void deleteTaskById(Realm realm, String id){
        final RealmResults<Task> result = realm.where(Task.class).equalTo("id",id).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                result.deleteFirstFromRealm();
            }
        });
    }

    public void setTaskComplete(Realm realm, String id){
        final Task task = getTaskById(realm, Integer.parseInt(id));
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                task.setComplete(true);
            }
        });
    }

    public Task getTaskById(Realm realm, int id){
        RealmResults<Task> result = realm.where(Task.class).equalTo("id",Integer.toString(id)).findAll();

        return result.last();
    }

    public RealmResults<Task> findAll(Realm realm){
        return realm.where(Task.class)
                .equalTo("complete",false)
                .findAllSorted("id", Sort.ASCENDING);
    }

    public RealmResults<Task> findAllCompleted(Realm realm){
        return realm.where(Task.class)
                .equalTo("complete",true)
                .findAllSorted("id", Sort.ASCENDING);
    }

    public Task findLastTask(Realm realm){
        RealmResults<Task> result = realm.where(Task.class).findAll();
        Task task = result.last();
        return task;
    }

    public static void initCountTable(Realm realm){

        final RealmResults<IdCount> result = realm.where(IdCount.class).findAll();
        if(result.size()==0){
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(new IdCount());
                }
            });
        }

    }

    public void countTableIncrement(Realm realm){
        final RealmResults<IdCount> result = realm.where(IdCount.class).findAll();

        if(result.size()==0){
            initCountTable(realm);
        }else{
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    result.last().countIncreasement();
                }
            });
        }



    }

    public int getIdCount(Realm realm){
        RealmResults<IdCount> result = realm.where(IdCount.class).findAll();
        return result.last().getCount();
    }

    //show personRealmList data
    public void showPeronsReal(Realm realm, int id){
        Task task = getTaskById(realm,id);
        task.showList();
    }

    public void realmToLocalPersonList(Realm realm, int id, List<Person> personList){
        Task task = getTaskById(realm,id);
        List<PersonRealm> dbList = task.getPersonList();

        for(PersonRealm dbPerson:dbList){
            Person localPerson = new Person(dbPerson);
            personList.add(localPerson);
        }
    }

    public List<Task> getTitleByDate(Realm realm, String month, String day, String year){
        RealmResults<Task> result = realm.where(Task.class)
                .equalTo("month",month)
                .equalTo("day", day)
                .equalTo("year",year)
                .findAll();

        for (Task task:result){
            Log.e("Error", "getTitleByMonth result: " + task.getTitle() + " month: " +task.getMonth());
        }

        return result;

    }

    public List<Task> getTitleByType(Realm realm, String type){
        if (type=="default"){
            return findAll(realm);
        }else{
            RealmResults<Task> result = realm.where(Task.class)
                    .equalTo("type",type)
                    .equalTo("complete",false)
                    .findAll();
            return result;
        }


    }
}
