package com.example.richa.todolist.RealmDBOperation;

import com.example.richa.todolist.MenuRecyclerView.Task;

import java.util.Calendar;

/**
 * Created by richa on 5/20/2017.
 */

public class TaskNotRealmObject {

    String id;
    String title;
    String head;
    String content;

    boolean notificationOn;
    String dateString;
    String month;
    String day;
    String year;
    String timeString;
    boolean navigation;
    String location;
    boolean locationOn;
    String type;
    boolean complete;


    public TaskNotRealmObject(Task realTask){
        id = realTask.getId();
        title = realTask.getTitle();
        head = realTask.getHead();
        content = realTask.getContent();
        notificationOn = realTask.getNotification();
        dateString = realTask.getDateString();
        month = realTask.getMonth();
        year = realTask.getYear();
        day = realTask.getDay();
        timeString = realTask.getTimeString();
        navigation = realTask.getNavigation();
        location = realTask.getLocation();
        locationOn = realTask.getLocationOn();
        type = realTask.getType();
        complete = realTask.getComplete();
    }

    public TaskNotRealmObject(String id){
        this.id = id;
        title = null;
        head = null;

        this.notificationOn=false;
        dateString = null;
        month = null;
        day = null;
        year = null;

        timeString = null;


        navigation = false;
        location = null;
        locationOn = false;
        type = "default";
        complete = false;
    }



    public void setTitle(String title){
        this.title = title;
        if(title.length()>0) {
            this.head = title.substring(0,1).toUpperCase();
        }else{
            head = " ";
        }
    }

    public void setId(String id){
        this.id = id;
    }
    public void setContent(String content) {this.content=content;}

    public void setNotification(boolean input){
        this.notificationOn = input;
    }
    public void setMonth(String input){month = input;}
    public void setYear(String input){year = input;}
    public void setDay(String input){day = input;}

    public void setDateString(String input){
        dateString = input;
    }
    public void setTimeString(String input){timeString=input;}


    public void setLocation(String location){
        this.location = location;
    }

    public void setLocationOn(boolean input){
        this.locationOn = input;
    }
    public void setType(String input){type = input;}
    public void setComplete(boolean input){complete = input;}


    //getters
    public boolean getNotification(){
        return notificationOn;
    }
    public String getDateString(){return dateString;}
    public String getMonth(){return month;}
    public String getDay(){return day;}
    public String getYear(){return year;}


    public String getTimeString(){return timeString;}

    public String getTitle(){
        return title;
    }
    public String getId(){
        return this.id;
    }
    public String getContent(){return content;}
    public String getHead(){
        return head;
    }
    public String getLocation(){return location;}
    public boolean getLocationOn(){return this.locationOn;}
    public String getType(){return type;}
    public boolean getComplete(){return complete;}

}
