package com.example.richa.todolist.GeneralFunction;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.ImageView;

import com.example.richa.todolist.MenuRecyclerView.Task;
import com.example.richa.todolist.PersonContactData.Person;
import com.example.richa.todolist.PersonContactData.PersonRealm;
import com.example.richa.todolist.R;

import java.util.List;

/**
 * Created by richa on 5/31/2017.
 */

public class GeneralFunctions {


    public GeneralFunctions(){}
    /**
     * return month in String
     * @param input
     */
    public String monthIntToString(int input){
        switch (input){
            case 0:return "Jan";
            case 1:return "Feb";
            case 2:return "Mar";
            case 3:return "Apr";
            case 4:return "May";
            case 5:return "Jun";
            case 6:return "Jul";
            case 7:return "Aug";
            case 8:return "Sep";
            case 9:return "Oct";
            case 10:return "Nov";
            case 11:return "Dec";
            default:return "";

        }
    }

    public int monthStringToInt(String input) {
        switch (input) {
            case "Jan":
                return 0;
            case "Feb":
                return 1;
            case "Mar":
                return 2;
            case "Apr":
                return 3;
            case "May":
                return 4;
            case "Jun":
                return 5;
            case "Jul":
                return 6;
            case "Aug":
                return 7;
            case "Sep":
                return 8;
            case "Oct":
                return 9;
            case "Nov":
                return 10;
            case "Dec":
                return 11;
            default:
                return -1;

        }
    }

    public void setImageviewBackground(String type, ImageView imageView){
        if (type!=null){
            switch (type){
                case "default":  imageView.setImageResource(R.drawable.gray_circle);
                    break;
                case "personal": imageView.setImageResource(R.drawable.cyan_circle);
                    break;
                case "work" : imageView.setImageResource(R.drawable.red_circle);
                    break;
                case "school":  imageView.setImageResource(R.drawable.yellow_circle);
                    break;
                case "fun" : imageView.setImageResource(R.drawable.magenta_circle);
                    break;
                default:  imageView.setImageResource(R.drawable.gray_circle);
            }
        }
    }

    public void navigation(Context context, String location){
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + location);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        context.startActivity(mapIntent);
    }

    public void smsAll(Context context, List<PersonRealm> taskList, String title, String content){
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);

        String body = title + "\r\n" + content;
        sendIntent.putExtra("sms_body", body);

        StringBuilder address = new StringBuilder();
        char separator = ';';
        if(Build.MANUFACTURER.equalsIgnoreCase("Samsung")) separator=',';

        for(PersonRealm person:taskList){
            address.append(person.number+separator);
        }

        sendIntent.putExtra("address", address.toString());

        sendIntent.setType("vnd.android-dir/mms-sms");
        context.startActivity(sendIntent);
    }
}
