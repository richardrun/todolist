package com.example.richa.todolist.RealmDBOperation;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by richa on 5/18/2017.
 */

public class IdCount extends RealmObject {
    @PrimaryKey
    String countTable;

    private int  count;
    public IdCount(){
        count = 0;
    }

    public int getCount(){
        return count;
    }
    public void countIncreasement(){
        count++;
    }
}
