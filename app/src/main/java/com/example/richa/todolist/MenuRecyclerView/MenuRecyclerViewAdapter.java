package com.example.richa.todolist.MenuRecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.richa.todolist.ContentInput;
import com.example.richa.todolist.GeneralFunction.GeneralFunctions;
import com.example.richa.todolist.Menu;
import com.example.richa.todolist.PersonContactData.Person;
import com.example.richa.todolist.PersonContactData.PersonRealm;
import com.example.richa.todolist.R;

import java.util.List;

/**
 * Created by richa on 5/15/2017.
 */

public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.TaskViewHolder>{
    public static class TaskViewHolder extends RecyclerView.ViewHolder{

        CardView cv;
        TextView title;
        TextView header;
        Context context;
        LinearLayout optionBar;
        ImageView imageView;
        ImageView sms;
        ImageView nav;
        ImageView downArrow;
        TaskViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();

            cv = (CardView)itemView.findViewById(R.id.menu_cv);
            title = (TextView) itemView.findViewById(R.id.menu_cv_title);
            header = (TextView) itemView.findViewById(R.id.menu_cv_header);
            optionBar = (LinearLayout) itemView.findViewById(R.id.menu_cv_option_bar);
            imageView = (ImageView) itemView.findViewById(R.id.menu_cardview_circle);
            nav = (ImageView) itemView.findViewById(R.id.cardview_nav);
            sms = (ImageView) itemView.findViewById(R.id.cardview_sms);
            downArrow = (ImageView) itemView.findViewById(R.id.cardview_down_arrow);
        }
    }

    List<Task> taskList;
    GeneralFunctions genFunc = new GeneralFunctions();

    public MenuRecyclerViewAdapter(List<Task> taskList){
        this.taskList = taskList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public MenuRecyclerViewAdapter.TaskViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_cardview, viewGroup, false);
        MenuRecyclerViewAdapter.TaskViewHolder pvh = new MenuRecyclerViewAdapter.TaskViewHolder(v);

        return pvh;
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {

        final Context context = holder.context;
        final int positionFinal = position;
        final LinearLayout opntionBar = holder.optionBar;
        final List<PersonRealm> personList = taskList.get(position).getPersonList();
        final String title = taskList.get(position).getTitle();
        final String content = taskList.get(position).getContent();

       // holder.itemView.setBackgroundColor(null);

        holder.header.setText(taskList.get(position).head);
        //show action bar animation
        holder.optionBar.setAlpha(0);
        holder.downArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(opntionBar.getAlpha()==0){
                    Log.e("ERROR", "inside optionbar on click");
                    opntionBar.setVisibility(View.VISIBLE);
                    opntionBar.animate()
                            .alpha(1)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    opntionBar.setAlpha(1);
                                }
                            });

                }else{
                    opntionBar.animate()
                            .alpha(0)
                            .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            opntionBar.setAlpha(0);
                            opntionBar.setVisibility(View.INVISIBLE);


                        }
                    });

                }


            }
        });

        holder.title.setText(taskList.get(position).title);


        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ContentInput.class);

                intent.putExtra("textClick", true);
                //intent.putExtra("task",taskList.get(positionFinal));
                //Log.e("RV onclick", taskList.get(positionFinal).id + "");
                intent.putExtra("id",Integer.parseInt(taskList.get(positionFinal).id));

                ((Activity) context).startActivityForResult(intent,0);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);

            }
        });

        //set circle color
        String type = taskList.get(position).type;
        Log.e("menu adapter", "type: " + type);
        genFunc.setImageviewBackground(type,holder.imageView);

        //sms icon listener
        holder.nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(taskList.get(positionFinal).getLocationOn()==true){
                    genFunc.navigation(context,taskList.get(positionFinal).getLocation());
                }else{
                    Toast.makeText(context,"location has not set",Toast.LENGTH_SHORT).show();
                }

            }
        });

        //sms icon listen
        holder.sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(personList.size()>0){
                    genFunc.smsAll(context,personList,title,content);
                }else{
                    Toast.makeText(context,"no contact added",Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public void add(String id, String title){
        taskList.add(new Task (id,title));
        notifyItemInserted(taskList.size()-1);
        notifyItemRangeChanged(taskList.size()-1,getItemCount());
    }

    public void add(Task task){
        taskList.add(task);
        notifyItemInserted(taskList.size()-1);
        notifyItemRangeChanged(taskList.size()-1,getItemCount());
    }

    public void deleteAll(){
        taskList.clear();
        notifyDataSetChanged();
        //notifyItemRangeChanged(0,taskList.size());
    }
}
