package com.example.richa.todolist.historyRecord;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.richa.todolist.MenuRecyclerView.MenuRecyclerViewAdapter;
import com.example.richa.todolist.MenuRecyclerView.Task;
import com.example.richa.todolist.R;
import com.example.richa.todolist.RealmDBOperation.RealmDBOperation;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by richa on 6/3/2017.
 */

public class HistoryActivity extends AppCompatActivity {

    //recyclerv view test
    private static List<Task> taskList;
    private MenuRecyclerViewAdapter adapter;
    private RecyclerView rv;


    Realm realm;
    public static RealmDBOperation realmDB;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        realm = Realm.getInstance(config);

        realmDB = RealmDBOperation.getInstance(realm);
        realmDB.initCountTable(realm);

        initializeData();
    }

    private void initializeData(){
        rv=(RecyclerView)findViewById(R.id.menu_rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        taskList = new ArrayList<>();

        adapter = new MenuRecyclerViewAdapter(taskList);
        rv.setAdapter(adapter);

        if(taskList.size()==0){
            List<Task> result = realmDB.findAllCompleted(realm);
            for(Task task:result){
                adapter.add(task);
            }
        }

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);

        itemTouchHelper.attachToRecyclerView(rv);
    }
    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.RIGHT){

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            realmDB.deleteTaskById(realm,taskList.get(position).getId());
            taskList.remove(position);

            adapter.notifyItemRemoved(position);
            adapter.notifyItemRangeChanged(position,adapter.getItemCount());
        }
    };

    /*
    @Override
    public void onBackPressed(){

    }
    */
 }
