package com.example.richa.todolist.PersonContactData;

import android.view.View;

import java.util.List;

/**
 * Created by richa on 5/11/2017.
 */

public class Person {
    public String name;
    public String number;
    public String title;
    public String content;

    public Person(String name, String number, String title, String content) {
        this.name = name;
        this.number = number;
        this.title = title;
        this.content = content;
    }

    public Person(PersonRealm personRealm){
        name = personRealm.getName();
        number = personRealm.getNumber();
        title = personRealm.getTitle();
        content = personRealm.getContent();
    }
}
