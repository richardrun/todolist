package com.example.richa.todolist;


import android.content.DialogInterface;
import android.content.Intent;

import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.richa.todolist.MenuRecyclerView.*;
import com.example.richa.todolist.RealmDBOperation.RealmDBOperation;
import com.example.richa.todolist.CalendarActivity.*;
import com.example.richa.todolist.historyRecord.HistoryActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Menu extends AppCompatActivity {

    private static final String ErrorTag = "Error";
    private static final String DEBUGTAG = "DEBUGTAG";
    private static final int SHOSW_CALC = 0;
    private TableLayout table;
    public static int idCount;
    private final String firstTableRow = "00";

    //
    public static NotificationAlarmFactory notificationFactory;

    //recyclerv view test
    private static List<Task> taskList;
    private MenuRecyclerViewAdapter adapter;
    private RecyclerView rv;

    //realmDB
    Realm realm;
    public static RealmDBOperation realmDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        //Realm DB
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        realm = Realm.getInstance(config);

        realmDB = RealmDBOperation.getInstance(realm);
        realmDB.initCountTable(realm);

        //notification factory singleton
        notificationFactory = NotificationAlarmFactory.getInstance();

        //add button, go to content page
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.myFAB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this,ContentInput.class);
                Log.e(ErrorTag,"idcount:  " +realmDB.getIdCount(realm));
                intent.putExtra("idCount", realmDB.getIdCount(realm));
                startActivityForResult(intent,SHOSW_CALC);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
            }
        });


        //RV test
        initializeData();

        //radio group init
        radioGroupFilterInit();
    }

    /**
     * content_input intent
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode==SHOSW_CALC){
            if (resultCode== RESULT_OK){
                //Log.e(ErrorTag, "data.getIntExtra(\"return\",-1): "+ data.getIntExtra("return",-1));


                // add new task
                if(data.getBooleanExtra("modify",false)==false) {
                    realmDB.countTableIncrement(realm);
                    adapter.add(realmDB.findLastTask(realm));
                }

                Log.e(ErrorTag, "outside idcount: "+ idCount);
                adapter.notifyDataSetChanged();

                Toast.makeText(Menu.this,"Task created",Toast.LENGTH_SHORT).show();

            }else Log.e(ErrorTag, "!resultCode==RESULT_OK");
        } else Log.e(ErrorTag, "!requestCode==SHOSW_CALC");


    }


    /**
     * cbox conformation dialog
     * @return
     */
    /*
    private void checkboxConfirmDialog(int id){

        final int finalI = id;
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(null);
        dialog.setMessage("Are you sure to delete the task?");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Log.e(ErrorTag, "check box true");
                deleteDbRowById(finalI);
                mapCbox.remove(finalI);
                mapTextView.remove(finalI);
                refresh();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e(ErrorTag, "check box false");
                mapCbox.get(finalI).setChecked(false);
            }
        });

        dialog.show();
    }
      */

    /**
     * recycler view init
     */
    private void initializeData(){
        rv=(RecyclerView)findViewById(R.id.menu_rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        taskList = new ArrayList<>();

        adapter = new MenuRecyclerViewAdapter(taskList);
        rv.setAdapter(adapter);

        if(taskList.size()==0){
            List<Task> result = realmDB.findAll(realm);
            for(Task task:result){
                adapter.add(task);
            }
        }

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);

        itemTouchHelper.attachToRecyclerView(rv);
    }
    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.RIGHT){

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition();
            //realmDB.deleteTaskById(realm,taskList.get(position).getId());

            AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
            builder.setTitle("Task completed?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    realmDB.setTaskComplete(realm,taskList.get(position).getId());
                    taskList.remove(position);

                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(position,adapter.getItemCount());

                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    adapter.notifyDataSetChanged();
                }
            });

            builder.show();


        }
    };


    public static List<Task> getTaskList(){
        return taskList;
    }

    @OnClick(R.id.calendarFAB)
    void startCalendarActivity(){

        startActivity(new Intent(this, CalendarActivity.class));
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }


    public void radioGroupFilterInit(){
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.filter_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                Log.e("ERROR", "id: " + checkedId);

                RadioButton button = (RadioButton) findViewById(checkedId);
                String buttonText = button.getTag().toString();
                Log.e("ERROR", "raodio button text: " + checkedId);

                switch (buttonText){
                    case "d":  radioGroupFilter("default");
                        break;
                    case "p": radioGroupFilter("personal");
                        break;
                    case "w":  radioGroupFilter("work");
                        break;
                    case "s":   radioGroupFilter("school");
                        break;
                    case "f":  radioGroupFilter("fun");
                        break;
                    default:
                }
            }
        });
    }

    public void radioGroupFilter(String type){
        adapter.deleteAll();

        List<Task> result = realmDB.getTitleByType(realm,type);
        Log.e("ERROR", "result size: " + result.size());
        if (result.size()>0){
            for(Task task:result){
                adapter.add(task);
            }
        }
    }

    @OnClick(R.id.historyButton)
    public void historyFAB(){
        startActivity(new Intent(this, HistoryActivity.class));
    }
}
