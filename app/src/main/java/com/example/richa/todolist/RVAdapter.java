package com.example.richa.todolist;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.richa.todolist.PersonContactData.Person;

import java.util.List;

/**
 * Created by richa on 5/11/2017.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder>{

    public static class PersonViewHolder extends RecyclerView.ViewHolder{

        CardView cv;
        TextView personName;
        TextView number;
        View view;
        ImageView imageView;
        PersonViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            cv = (CardView)itemView.findViewById(R.id.cv);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            number = (TextView)itemView.findViewById(R.id.number);
            imageView = (ImageView) itemView.findViewById(R.id.SMS);
        }

    }

    static List<Person> persons;

    RVAdapter(List<Person> persons){
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_cardview, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        final int position = i;

        personViewHolder.personName.setText(persons.get(i).name);
        personViewHolder.number.setText(persons.get(i).number);
        /*
        personViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                persons.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,getItemCount());
            }
        });
         */
        personViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);

                String body = persons.get(position).title + "\r\n" + persons.get(position).content;
                sendIntent.putExtra("sms_body", body);
                sendIntent.putExtra("address", persons.get(position).number);

                sendIntent.setType("vnd.android-dir/mms-sms");
                v.getContext().startActivity(sendIntent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return persons.size();
    }


    public static List<Person> getPersonsList(){
        return persons;
    }
}

