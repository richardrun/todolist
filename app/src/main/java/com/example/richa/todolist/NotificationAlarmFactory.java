package com.example.richa.todolist;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by richa on 5/1/2017.
 */

public class NotificationAlarmFactory {

    private static int alarmId;
    private static int notificationId;
    private String content;
    private Calendar cal;
    private static NotificationAlarmFactory notificationAlarmFactory = null;

    private NotificationAlarmFactory(){

    }

    public static NotificationAlarmFactory getInstance(){
        if(notificationAlarmFactory==null){
            notificationAlarmFactory = new NotificationAlarmFactory();
        }
        return notificationAlarmFactory;
    }

    public void createNotificationAlarm(Context context, int id, String content, Calendar cal){
        this.alarmId = id;
        this.notificationId = id;
        this.content = content;
        this.cal = cal;

        scheduleNotification(getNotification(this.content, context), context);
    }

    public void cancelNotificationAlarm(Context context, int id){
        this.alarmId = id;
        this.notificationId = id;

        Intent notificationIntent = new Intent(context, NotificationBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), alarmId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
        pendingIntent.cancel();
        alarmManager.cancel(pendingIntent);
    }

    private Notification getNotification(String content, Context context) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("To Do List Notification");
        builder.setContentText(content);
        builder.setSmallIcon(android.R.drawable.ic_dialog_alert);

        return builder.build();
    }

    private void scheduleNotification(Notification notification, Context context) {

        Intent notificationIntent = new Intent(context, NotificationBroadcastReceiver.class);
        notificationIntent.putExtra(NotificationBroadcastReceiver.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationBroadcastReceiver.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), alarmId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE),0);
        Log.e("ErrorTag", cal.getTime().toString());
        Log.e("ErrorTag", "Year: " + cal.get(Calendar.YEAR) + " MONTH: "+ cal.get(Calendar.MONTH) + " day: " + cal.get(Calendar.DAY_OF_MONTH) + " Hour: " + cal.get(Calendar.HOUR) + " minute: " + cal.get(Calendar.MINUTE));


        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                pendingIntent);

    }

}
