package com.example.richa.todolist.PersonContactData;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by richa on 5/24/2017.
 */

public class PersonRealm extends RealmObject {

    @PrimaryKey
    public String key;
    public String name;
    public String number;
    public String title;
    public String content;
    public String parentsID;

    public PersonRealm(){}

    public PersonRealm(Person person){
        name = person.name;
        number = person.number;
        title = person.title;
        content = person.content;
    }

    public void updateStates(Person person){
        name = person.name;
        number = person.number;
        title = person.title;
        content = person.content;
    }

    public void setKey(){
        key = name+parentsID;
    }

    public String getName(){return name;}
    public String getNumber(){return number;}
    public String getTitle(){return title;}
    public String getContent(){return content;}
}
