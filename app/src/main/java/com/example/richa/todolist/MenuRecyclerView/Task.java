package com.example.richa.todolist.MenuRecyclerView;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.example.richa.todolist.PersonContactData.PersonRealm;
import com.example.richa.todolist.RealmDBOperation.TaskNotRealmObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by richa on 5/15/2017.
 */

public class Task extends RealmObject implements Parcelable{

    @PrimaryKey
    String id;

    String title;
    String head;
    String content;
    boolean notificationOn;
    String dateString;
    String month;
    String day;
    String year;
    String timeString;
    boolean navigation;
    String location;
    boolean locationOn;
    String type;
    boolean complete;
    public RealmList<PersonRealm> personsRealmList;

    public Task(){
        personsRealmList = new RealmList<>();
    }

    public Task(String id, String title){
        this.id = id;
        this.title = title;
        if(title.length()>0) {
            this.head = title.substring(0,1).toUpperCase();
        }else{
            head = " ";
        }
        content = null;
        this.notificationOn=false;
        dateString = null;
        month = null;
        day = null;
        year = null;
        timeString = null;

        navigation = false;
        location = null;
        locationOn = false;
        personsRealmList = new RealmList<>();
        type = null;
        complete = false;
    }

    public void setId(String id){this.id=id;}
    public void setTitle(String title){
        this.title = title;
        if(title.length()>0) {
            this.head = title.substring(0,1).toUpperCase();
        }else{
            head = " ";
        }
    }
    public void setContent(String content) {this.content=content;}
    public void setNotification(boolean input){
        this.notificationOn = input;
    }
    public void setDateString(String input){
        dateString = input;
    }
    public void setMonth(String input){month = input;}
    public void setYear(String input){year = input;}
    public void setDay(String input){day = input;}

    public void setTimeString(String input){timeString=input;}

    public void setLocation(String location){
        this.location = location;
    }

    public void setLocationOn(boolean input){
        this.locationOn = input;
    }
    public void setType(String input){type = input;}
    public void setComplete(boolean input){complete = input;}

    //getters
    public String getTitle(){
        return title;
    }
    public String getHead(){
        return head;
    }
    public String getContent(){return content;}
    public boolean getNotification(){
        return notificationOn;
    }
    public String getDateString(){return dateString;}
    public String getMonth(){return month;}
    public String getDay(){return day;}
    public String getYear(){return year;}
    public String getTimeString(){return timeString;}
    public String getId(){
        return this.id;
    }
    //public Calendar getCal(){return this.cal;}
    public boolean getNavigation(){return navigation;}
    public String getLocation(){return location;}
    public boolean getLocationOn(){return this.locationOn;}
    public String getType(){return type;}
    public boolean getComplete(){return complete;}

    public List getPersonList(){
        return personsRealmList;
    }

    public void showList(){
        for(PersonRealm person:personsRealmList){
            Log.e("ERROR", person.getName());
        }
    }


    public Task (Parcel parcel) {
        this.id = parcel.readString();
        this.title = parcel.readString();
        this.head = parcel.readString();
        this.notificationOn = parcel.readInt()==1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(head);
        dest.writeInt(notificationOn?1:0);

    }

    public static Creator<Task> CREATOR = new Creator<Task>() {

        @Override
        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }

    };

}
