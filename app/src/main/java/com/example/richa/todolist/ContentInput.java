package com.example.richa.todolist;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.richa.todolist.GeneralFunction.GeneralFunctions;
import com.example.richa.todolist.MenuRecyclerView.Task;
import com.example.richa.todolist.PersonContactData.Person;
import com.example.richa.todolist.RealmDBOperation.RealmDBOperation;
import com.example.richa.todolist.RealmDBOperation.TaskNotRealmObject;
import com.example.richa.todolist.map.MapFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by richa on 4/8/2017.
 */

public class ContentInput extends AppCompatActivity{

    final int textSize = 15;
    private String ErrorTag = "ERROR";
    private int id;
    private String title;
    boolean textClick;
    protected static Calendar cal;
    GeneralFunctions genFunc = new GeneralFunctions();

    public NotificationAlarmFactory notificationFactory;

    // recycler view
    private List<Person> persons;
    private RecyclerView rv;
    RVAdapter adapter;

    //butter knife
    @BindView(R.id.title) EditText titleEditView;
    @BindView(R.id.content) EditText contentEditView;
    @BindView(R.id.type_spinner) Spinner typeSpinner;

    //notification
    @BindView(R.id.notification) Switch notificationSwitch;
    @BindView(R.id.date) TextView dateTextView;
    @BindView(R.id.time) TextView timeTextView;
    @BindView(R.id.date_icon) ImageView dateIcon;
    @BindView(R.id.clock_icon) ImageView clockIcon;


    //location & navigation
    @BindView(R.id.switch_nav) Switch locationSwitch;
    @BindView(R.id.location_input) TextView locationNameDisplay;
    @BindView(R.id.location) RelativeLayout locationBar;
    //Task info
    Task taskInfo;


    //realm  && DB
    Realm realm;
    public static RealmDBOperation realmDB;
    TaskNotRealmObject taskTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_input);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        realm = Realm.getInstance(config);
        //realm = Realm.getDefaultInstance();
        realmDB = RealmDBOperation.getInstance(realm);

        ButterKnife.bind(this);

        notificationFactory = NotificationAlarmFactory.getInstance();
        cal = Calendar.getInstance();

        textClick = getIntent().getBooleanExtra("textClick",false);
        if(textClick ==true){
            Log.e(ErrorTag,"true");
            id = getIntent().getIntExtra("id",-1);
            title = ((EditText) findViewById(R.id.title)).getText().toString();


            taskInfo = realmDB.getTaskById(realm,id);
            taskTemp = new TaskNotRealmObject(taskInfo);

            setUpContentPage(id);
            Log.e(ErrorTag,"intent get parcelable extra: " + taskInfo.getTitle());

            //test realm db person list content
            realmDB.showPeronsReal(realm,id);
            persons = new ArrayList<>();
            realmDB.realmToLocalPersonList(realm,id,persons);

        }else{
            id = getIntent().getIntExtra("idCount",-1);
            Log.e(ErrorTag,"text click false id:" + id);
            String stringId = Integer.toString(id);
            taskTemp = new TaskNotRealmObject(stringId);

            persons = new ArrayList<>();
        }

        //edit text hide keyboard when out of focus
        EditText title = (EditText) findViewById(R.id.title);
        title.setOnFocusChangeListener(new hiddenKeyboard());
        EditText content = (EditText) findViewById(R.id.content);
        content.setOnFocusChangeListener(new hiddenKeyboard());

        //notification switch button
        notiSwitch();

        //recycler view initialize
        initializeData();

        FloatingActionButton save = (FloatingActionButton) findViewById(R.id.contentFAB);
        save.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String title = ((EditText) findViewById(R.id.title)).getText().toString();

                //id = textClick?tempID:getIntent().getIntExtra("idCount",-1);
                //createTable(db, storageTable);

                Log.e(ErrorTag,"intent id :" + id);

                if(title.trim().equals("")){
                    Toast.makeText(ContentInput.this,"title field is requied to save", Toast.LENGTH_SHORT).show();
                }else{
                    if(textClick==true){
                        updateDb(id, titleEditView.getText().toString());
                    }else{
                        insertDb(id,title);
                    }

                    //enable notification after saved
                    Switch notiSwitch = (Switch) findViewById(R.id.notification);
                    if(notiSwitch.isChecked()){
                        notificationFactory.createNotificationAlarm(ContentInput.this,id,title,cal);
                    }else{
                        try{
                            notificationFactory.cancelNotificationAlarm(ContentInput.this,id);
                        }catch (Exception e){
                            Log.e(ErrorTag,e.toString());
                        }
                    }

                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                }



            }
        });

        //delete sql

        ImageButton addContact = (ImageButton) findViewById(R.id.add_contact);
        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(ErrorTag,"add contact button listen");
                calContctPickerFnc();
            }
        });


    }

    private void setUpContentPage(int id){

        //Task task = realmDB.getTaskById(realm,id);
        EditText editText = (EditText) findViewById(R.id.title);
        editText.setText(taskTemp.getTitle());

        contentEditView.setText(taskTemp.getContent());

        if(taskTemp.getLocationOn()==true){
            locationSwitch.setOnCheckedChangeListener(null);
            locationSwitch.setChecked(true);
            locationNameDisplay.setText(taskTemp.getLocation());
            locationBar.setVisibility(View.VISIBLE);
            ButterKnife.bind(this);

            //testing fragment
            createMapFragment(taskTemp.getLocation());

        }

        if (taskTemp.getNotification()==true){
            notificationSwitch.setChecked(true);
        }
        if(taskTemp.getDateString()!=null){
            dateTextView.setText(taskTemp.getDateString());
            dateTextView.setVisibility(View.VISIBLE);
        }
        if(taskTemp.getTimeString()!=null){
            timeTextView.setText(taskTemp.getTimeString());
            timeTextView.setVisibility(View.VISIBLE);
        }

        String type = taskTemp.getType();
        switch (type){
            case "default":break;
            case "personal":  typeSpinner.setSelection(1);
                break;
            case "work":typeSpinner.setSelection(2);
                break;
            case "school": typeSpinner.setSelection(3);
                break;
            case "fun":  typeSpinner.setSelection(4);
                break;
        }

    }

    private void updateDb(int id, String title){
        try{
            //realmDB.updateTitle(realm,Menu.getTaskList(),id,titleEditView.getText().toString());
            taskTemp.setTitle(title);
            taskTemp.setContent(contentEditView.getText().toString());
            //Log.e(ErrorTag, "taskTemp title: " + taskTemp.getTitle());
            //Log.e(ErrorTag, "taskTemp notification: " + taskTemp.getNotification());

            realmDB.updateAll(realm,taskTemp);
            ///////////////

            getIntent().putExtra("modify", true);
            setResult(RESULT_OK,getIntent());

        }catch (Exception e){
            getIntent().putExtra("modify", false);
            Log.e(ErrorTag, e.getMessage());
            Log.e(ErrorTag, "updatedb fails");

        }
    }

    private void insertDb(int id, String title){
        ////
        taskTemp.setId(Integer.toString(id));
        taskTemp.setTitle(title);
        taskTemp.setContent(contentEditView.getText().toString());
        realmDB.add(realm,Integer.toString(id),taskTemp);

        //realmDB.showPeronsReal(realm,id);

        setResult(RESULT_OK,getIntent());
    }

    /**
     * date picker
     */
    private void datePicker(){
        //Calendar datePickercal = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(ContentInput.this, new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.e(ErrorTag, "date picker: " + year + " " + month + " " + dayOfMonth);
                cal.set(year,month,dayOfMonth);
                //timePicker();
                TextView date = (TextView) findViewById(R.id.date);
                date.setVisibility(View.VISIBLE);
                String monthTemp = monthString(cal.get(Calendar.MONTH)+1);
                String dateString = cal.get(Calendar.DAY_OF_MONTH) + " " + monthTemp + " "+cal.get(Calendar.YEAR);
                taskTemp.setDateString(dateString);
                taskTemp.setMonth(monthTemp);
                taskTemp.setDay(Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
                taskTemp.setYear(Integer.toString(cal.get(Calendar.YEAR)));

                Log.e("content input datepick", "taskTemp year: " + taskTemp.getYear());
                date.setText(dateString);
            }
        }
        ,cal.get(Calendar.YEAR)
        ,cal.get(Calendar.MONTH)
        ,cal.get(Calendar.DAY_OF_MONTH));

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //notificationSwitch.setChecked(false);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                notificationSwitch.setChecked(false);
                dialog.dismiss();
                return true;
            }
        });
        dialog.show();
    }

    /**
     * Time picker
     */

    private void timePicker(){
        TimePickerDialog dialog = new TimePickerDialog(ContentInput.this,new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.e(ErrorTag, "time picker: " + hourOfDay + " " + minute + " ");

                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                cal.set(Calendar.MINUTE,minute);
                cal.set(Calendar.SECOND,0);
                TextView noti = (TextView) findViewById(R.id.time);
                setDateAndTime(cal);
                //taskTemp.setNotification(true);
                noti.setVisibility(View.VISIBLE);


                Log.e(ErrorTag, "time picker id: " + id);

                //Menu.notificationFactory.createNotificationAlarm(ContentInput.this,id,title,cal);

            }

        }
        ,0,0,true);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //notificationSwitch.setChecked(false);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                //notificationSwitch.setChecked(false);
                dialog.dismiss();
                return true;
            }
        });
        dialog.show();

    }

    /**
     * hidden keyboard when edit text is not focus
     * implement onFocusListen
     */
    class hiddenKeyboard implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus){
                InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                //title = ((EditText) findViewById(R.id.title)).getText().toString();
            }
        }
    }

    private void setDateAndTime(Calendar cal){
        //taskTemp.setNotification(true);
        //Log.e(ErrorTag, "content page taskinfo notification: " + taskInfo.getNotification());


        TextView time = (TextView) findViewById(R.id.time);
        int temp = cal.get(Calendar.MINUTE);
        String timeString;
        if (temp ==0){
            timeString = cal.get(Calendar.HOUR_OF_DAY) + ":00";

        }else{
            timeString = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        }
        taskTemp.setTimeString(timeString);


        time.setText(timeString);



        //

    }


    /**
     * transform integer month to String
     * @param month
     * @return
     */
    public String monthString(int month){
        switch (month){
            case 1: return "Jan";
            case 2: return "Feb";
            case 3: return "Mar";
            case 4: return "Apr";
            case 5: return "May";
            case 6: return "Jun";
            case 7: return "Jul";
            case 8: return "Aug";
            case 9: return "Sep";
            case 10: return "Oct";
            case 11: return "Nov";
            case 12: return "Dec";

            default:return "";
        }
    }

    private void notiSwitch(){
        Switch notiSwitch = (Switch) findViewById(R.id.notification);
        notiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RelativeLayout noti = (RelativeLayout) findViewById(R.id.schedule);
                if(isChecked){
                    //datePicker();
                    //noti.setVisibility(View.VISIBLE);
                    taskTemp.setNotification(true);
                }else{
                    //noti.setVisibility(View.GONE);
                    taskTemp.setNotification(false);
                }
            }
        });
    }

    /**
     * contact picker
     */

    void calContctPickerFnc()
    {
        Log.e(ErrorTag,"contact intent");
        Intent calContctPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        calContctPickerIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(calContctPickerIntent, 1);
    }


    /**
     * result from contack picker
     * @param reqCode 1 for contact list
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data)
    {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode)
        {
            case (1) :
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contctDataVar = data.getData();

                    Cursor contctCursorVar = getContentResolver().query(contctDataVar, null,
                            null, null, null);
                    if (contctCursorVar.getCount() > 0)
                    {
                        while (contctCursorVar.moveToNext())
                        {
                            String ContctUidVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts._ID));

                            String ContctNamVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                            Log.e("Names", ContctNamVar);
                            String ContctMobVar = "";
                            if (Integer.parseInt(contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                            {
                                // Query phone here. Covered next
                                ContctMobVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                Log.e("Number", ContctMobVar);
                            }

                            //addPerson(ContctNamVar,ContctMobVar, titleEditView.getText().toString(),contentEditView.getText().toString());
                            addPerson(ContctNamVar,ContctMobVar,titleEditView.getText().toString(), contentEditView.getText().toString());

                        }
                    }
                }
                break;
        }
    }


    /**
     * navigation switch button   | Location switch
     */
    @OnCheckedChanged(R.id.switch_nav)
    public void locationSwitch(CompoundButton buttonView, boolean isChecked){
        if(isChecked){
            showTextDialog();
        } else if(!isChecked){
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.location);
            rl.setVisibility(View.GONE);
            taskTemp.setLocationOn(false);
        }
    }

    /**
     * text dialog( for navigation input)
     */
    public void showTextDialog(){
        final EditText editText = new EditText(this);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Input the location")
                .setView(editText)
                .setPositiveButton("OK",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TextView navText = (TextView) findViewById(R.id.location_input);
                        String locationName = editText.getText().toString();
                        navText.setText("Your input: " + locationName);

                        createMapFragment(locationName);

                        RelativeLayout rl = (RelativeLayout) findViewById(R.id.location);
                        rl.setVisibility(View.VISIBLE);
                        taskTemp.setLocationOn(true);
                        taskTemp.setLocation(locationName);

                        //testing fragment

                    }
                });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK){
                    locationSwitch.setChecked(false);
                    dialog.dismiss();
                }
                return true;
            }
        });
        dialog.show();
    }


    public void createMapFragment(String destination){

        Bundle bundle = new Bundle();
        bundle.putString("dest", destination);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment mapFrag = new MapFragment();
        mapFrag.setArguments(bundle);
        fragmentTransaction.add(R.id.fragment_container,mapFrag);
        fragmentTransaction.commit();
    }


    /**
     * initialize  recycler view
     */
    private void initializeData(){
        rv=(RecyclerView)findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        //rv.setHasFixedSize(true);

        adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);

        itemTouchHelper.attachToRecyclerView(rv);
    }

    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.RIGHT){
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

            return true;
        }
        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            persons.remove(position);
            adapter.notifyItemRemoved(position);
            adapter.notifyItemRangeChanged(position,adapter.getItemCount());
        }
    };

    /**
     * add person to sms from contact list
     * @param name
     * @param number
     * @param title
     * @param content
     */
    private void addPerson(String name, String number, String title, String content){
        persons.add(new Person(name, number, title, content));

        Log.e(ErrorTag,"persons size: " + persons.size());
        adapter.notifyItemInserted(persons.size()-1);
        adapter.notifyItemRangeChanged(persons.size()-1,adapter.getItemCount());
        adapter.notifyDataSetChanged();

    }

    @OnClick(R.id.sms_all)
    public void smsAll(){
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);

        String body = titleEditView.getText().toString() + "\r\n" + contentEditView.getText().toString();
        sendIntent.putExtra("sms_body", body);

        StringBuilder address = new StringBuilder();
        char separator = ';';
        if(Build.MANUFACTURER.equalsIgnoreCase("Samsung")) separator=',';

        for(Person person:persons){
            address.append(person.number+separator);
        }

        sendIntent.putExtra("address", address.toString());

        sendIntent.setType("vnd.android-dir/mms-sms");
        startActivity(sendIntent);

    }

    @OnClick(R.id.date_icon)
    void dateSet(){
        datePicker();
    }

    @OnClick(R.id.clock_icon)
    void clockSet(){
        timePicker();
    }


    /**
     * back press event
     */
    @Override
    public void onBackPressed(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit without save?");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ContentInput.super.onBackPressed();
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();

    }

    @OnItemSelected(R.id.type_spinner)
    public void typeOnSelectListener(int position){
        switch (position){
            case 0: Log.e(ErrorTag,position+" "+typeSpinner.getItemAtPosition(position).toString());
                typeSpinner.setBackgroundColor(Color.GRAY);
                taskTemp.setType("default");
                break;
            case 1: Log.e(ErrorTag,position+"");
                typeSpinner.setBackgroundColor(Color.CYAN);
                taskTemp.setType("personal");
                break;
            case 2: Log.e(ErrorTag,position+"");
                typeSpinner.setBackgroundColor(Color.RED);
                taskTemp.setType("work");
                break;
            case 3: Log.e(ErrorTag,position+"");
                typeSpinner.setBackgroundColor(Color.YELLOW);
                taskTemp.setType("school");
                break;
            case 4: Log.e(ErrorTag,position+"");
                typeSpinner.setBackgroundColor(Color.MAGENTA);
                taskTemp.setType("fun");
                break;
        }
    }
}
